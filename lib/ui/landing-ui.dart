import 'package:flutter/material.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

import 'login-ui.dart';

class LandingUI extends StatefulWidget {
  @override
  _LandingUIState createState() => _LandingUIState();
}

class _LandingUIState extends State<LandingUI> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
              flex: 1,
              child: Container(
                color: Colors.white,
                child: Image.asset('assets/sample-logo.jpg'),
              )),
          Expanded(
              flex: 1,
              child: Container(
                color: Colors.white,
                width: double.infinity,
                child: Container(
                  margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.all(30.0),
                  decoration: BoxDecoration(
                    color: Colors.cyan[700],
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(70.0),
                      topLeft: Radius.circular(70.0),
                    ),
                  ),
                  child: ListView(
                    physics: NeverScrollableScrollPhysics(),
                      children: [
                        Text(
                          'Welcome',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 35,
                              color: Colors.white),
                        ),
                        SizedBox(height: 15),
                        Text(
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              letterSpacing: 0.5),
                        ),
                        SizedBox(height: 55),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 60,
                                child: ElevatedButton(
                                  onPressed: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (ctx) => LoginUI()));
                                  },
                                  child: Text(
                                    'Sign In',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              Colors.orange),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(24.0),
                                      ))),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 60,
                                child: ElevatedButton(
                                  onPressed: () {
                                    AwesomeDialog(
                                      context: context,
                                      dialogType: DialogType.ERROR, 
                                      animType: AnimType.BOTTOMSLIDE,
                                      title: 'ERROR',
                                      desc: 'This feature has been disabled',
                                      btnCancelOnPress: () {},
                                      btnCancelText: 'OK'
                                    )..show();
                                  },
                                  child: Text(
                                    'Sign Up',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              Colors.white),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(24.0),
                                      ))),
                                ),
                              ),
                            ),
                          ],
                        )
                      ]),
                ),
              )),
        ],
      ),
    );
  }
}
