import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:tester_project/presenter/auth/login-presenter.dart';
import 'package:tester_project/ui/main-menu.dart';
import 'package:tester_project/view/auth/login-view.dart';

class LoginUI extends StatefulWidget {
  @override
  _LoginUIState createState() => _LoginUIState();
}

class _LoginUIState extends State<LoginUI> implements LoginView {
  LoginPresenter? _presenter;
  bool _passwordVisible = true;
  var _username = TextEditingController();
  var _password = TextEditingController();

  @override
  void initState() {
    _presenter = LoginPresenter(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    // var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.cyan[700],
      appBar: AppBar(
        backgroundColor: Colors.cyan[700],
        elevation: 0,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            )),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              padding: EdgeInsets.all(24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Sign In',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 28,
                        color: Colors.white),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 16,
                        color: Colors.white),
                  ),
                ],
              )),
          SizedBox(height: 20),
          Expanded(
              child: Container(
            padding: EdgeInsets.only(top: 55.0, left: 30, right: 30),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(45.0),
                topLeft: Radius.circular(45.0),
              ),
            ),
            child: NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (OverscrollIndicatorNotification overscroll) {
                overscroll.disallowGlow();
                return true;
              },
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  TextField(
                    controller: _username,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        hintStyle: TextStyle(color: Colors.grey[800]),
                        hintText: "Username",
                        contentPadding: EdgeInsets.all(25),
                        fillColor: Colors.grey[200]),
                  ),
                  SizedBox(height: 20),
                  TextField(
                    controller: _password,
                    obscureText: _passwordVisible,
                    decoration: InputDecoration(
                        suffixIcon: IconButton(
                            icon: _passwordVisible
                                ? Icon(
                                    Icons.visibility_off,
                                    color: Colors.cyan[700],
                                  )
                                : Icon(
                                    Icons.visibility,
                                    color: Colors.cyan[700],
                                  ),
                            onPressed: () {
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            }),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        hintStyle: TextStyle(color: Colors.grey[800]),
                        hintText: "Password",
                        contentPadding: EdgeInsets.all(25),
                        fillColor: Colors.grey[200]),
                  ),
                  SizedBox(height: 30),
                  Container(
                    height: 60,
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: () {
                        _presenter?.onLogin(_username.text, _password.text);
                      },
                      child: Text(
                        'Sign In',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.cyan[700]),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24.0),
                          ))),
                    ),
                  ),
                  SizedBox(height: 30),
                  Container(
                    height: 60,
                    width: width / 1.20,
                    child: ElevatedButton(
                      onPressed: () {
                        AwesomeDialog(
                            context: context,
                            dialogType: DialogType.ERROR,
                            animType: AnimType.BOTTOMSLIDE,
                            title: 'ERROR',
                            desc: 'This feature has been disabled',
                            btnCancelOnPress: () {},
                            btnCancelText: 'OK')
                          ..show();
                      },
                      child: ListTile(
                        leading: Image.asset(
                          'assets/google.png',
                          height: 30,
                        ),
                        title: Text(
                          'Continue with Google',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                        trailing:
                            Icon(Icons.arrow_right_alt, color: Colors.black),
                      ),
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.white),
                          elevation: MaterialStateProperty.all(5),
                          shadowColor: MaterialStateProperty.all(Colors.black),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24.0),
                          ))),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    height: 60,
                    width: width / 1.20,
                    child: ElevatedButton(
                      onPressed: () {
                        AwesomeDialog(
                            context: context,
                            dialogType: DialogType.ERROR,
                            animType: AnimType.BOTTOMSLIDE,
                            title: 'ERROR',
                            desc: 'This feature has been disabled',
                            btnCancelOnPress: () {},
                            btnCancelText: 'OK')
                          ..show();
                      },
                      child: ListTile(
                        leading: Image.asset(
                          'assets/facebook.png',
                          height: 30,
                        ),
                        title: Text(
                          'Continue with Facebook',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                        trailing:
                            Icon(Icons.arrow_right_alt, color: Colors.black),
                      ),
                      style: ButtonStyle(
                          elevation: MaterialStateProperty.all(5),
                          shadowColor: MaterialStateProperty.all(Colors.black),
                          backgroundColor:
                              MaterialStateProperty.all(Colors.white),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24.0),
                          ))),
                    ),
                  ),
                ],
              ),
            ),
          ))
        ],
      ),
    );
  }

  @override
  void onFinishLoadingLogin() {
    EasyLoading.dismiss();
  }

  @override
  void onNextAcivity() {}

  @override
  void onStartLoadingLogin() {
    EasyLoading.show(
      status: 'loading...',
      maskType: EasyLoadingMaskType.black,
    );
  }

  @override
  void showMessageFailed(String value) {
    AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: 'ERROR',
        desc: value,
        btnCancelOnPress: () {},
        btnCancelText: 'OK')
      ..show();
  }

  @override
  void showMessageSuccess(String value) {
    AwesomeDialog(
        dismissOnTouchOutside: false,
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: 'BERHASIL',
        desc: value,
        btnOkText: "OK",
        btnOkOnPress: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (ctx) => MainMenu()));
        })
      ..show();
  }
}
