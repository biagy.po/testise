import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:intl/intl.dart';
import 'package:like_button/like_button.dart';
import 'package:tester_project/presenter/main/main-menu-presenter.dart';
import 'package:tester_project/view/main/main-menu-view.dart';

class BolaProduct extends StatefulWidget {
  @override
  _BolaProductState createState() => _BolaProductState();
}

class _BolaProductState extends State<BolaProduct> implements MainMenuView {
  MainMenuPresenter? _presenter;

  @override
  void initState() {
    _presenter = MainMenuPresenter(this);
    _presenter?.onGetData().then((value) {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _presenter?.listItems == null
        ? Container()
        : Container(
            child: StaggeredGridView.countBuilder(
                padding: EdgeInsets.all(14),
                shrinkWrap: true,
                itemCount: _presenter?.listItems?.length,
                crossAxisCount: 4,
                staggeredTileBuilder: (index) => new StaggeredTile.fit(2),
                mainAxisSpacing: 8,
                crossAxisSpacing: 8,
                itemBuilder: (BuildContext context, int index) {
                  String mainString =
                      _presenter?.listItems?[index]["item_name"];
                  String subString = "Bola";
                  String subString2 = "bola";
                  if (mainString.contains(subString) ||
                      mainString.contains(subString2)) {
                    print("true");
                    if (_presenter?.listItems?[index]["image"] == null) {
                      final oCcy = new NumberFormat("#,##0", "id");
                      var amounts = oCcy.format(
                          (_presenter?.listItems?[index]["normal_rate"]));
                      var amounts2 = oCcy.format(
                          (_presenter?.listItems?[index]["promo_rate"]));
                      return Stack(
                        children: [
                          GestureDetector(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.white, width: 3),
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(15.0),
                                    child: ColorFiltered(
                                      colorFilter: ColorFilter.mode(
                                          Colors.black.withOpacity(0.1),
                                          BlendMode.darken),
                                      child: Hero(
                                          tag: '$index',
                                          child: Image.asset(
                                            "assets/default-no-image.png",
                                            fit: BoxFit.cover,
                                          )),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5),
                                Container(
                                    padding: EdgeInsets.only(left: 5),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            _presenter?.listItems?[index]
                                                        ["normal_rate"] !=
                                                    _presenter
                                                            ?.listItems?[index]
                                                        ["promo_rate"]
                                                ? Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        'Rp. $amounts2',
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 15.0),
                                                      ),
                                                      Text(
                                                        'Rp. $amounts',
                                                        style: TextStyle(
                                                            decoration:
                                                                TextDecoration
                                                                    .lineThrough,
                                                            color: Colors.grey,
                                                            fontSize: 13.0),
                                                      ),
                                                    ],
                                                  )
                                                : Text(
                                                    'Rp. $amounts',
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15.0),
                                                  ),
                                            LikeButton(
                                              size: 25,
                                              circleColor: CircleColor(
                                                  start: Color(0xff00ddff),
                                                  end: Color(0xff0099cc)),
                                              bubblesColor: BubblesColor(
                                                dotPrimaryColor:
                                                    Color(0xff33b5e5),
                                                dotSecondaryColor:
                                                    Color(0xff0099cc),
                                              ),
                                              likeBuilder: (bool isLiked) {
                                                return Icon(
                                                  isLiked
                                                      ? Icons.favorite
                                                      : Icons.favorite_outline,
                                                  color: isLiked
                                                      ? Colors.black
                                                      : Colors.grey,
                                                  size: 25,
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                        SizedBox(height: 3),
                                        Text(
                                          _presenter?.listItems?[index]
                                              ["item_name"],
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15.0),
                                        ),
                                      ],
                                    )),
                                SizedBox(height: 15)
                              ],
                            ),
                            onTap: () {
                              // Navigator.push(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (ctx) => WisataDetails(
                              //               modelWisata: [modelWisata.item[index]],
                              //               // images: images[index],
                              //             )));
                            },
                          ),
                        ],
                      );
                    } else {
                      String image = _presenter?.listItems?[index]["image"];
                      String images =
                          image.replaceAll("data:image/jpeg;base64,", "");
                      final oCcy = new NumberFormat("#,##0", "id");
                      var amounts = oCcy.format(
                          (_presenter?.listItems?[index]["normal_rate"]));
                      var amounts2 = oCcy.format(
                          (_presenter?.listItems?[index]["promo_rate"]));
                      return Stack(
                        children: [
                          GestureDetector(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.white, width: 3),
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(15.0),
                                    child: ColorFiltered(
                                      colorFilter: ColorFilter.mode(
                                          Colors.black.withOpacity(0.1),
                                          BlendMode.darken),
                                      child: Hero(
                                          tag: '$index',
                                          child: Image.memory(
                                            base64Decode(images),
                                            fit: BoxFit.cover,
                                          )),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5),
                                Container(
                                    padding: EdgeInsets.only(left: 5),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            _presenter?.listItems?[index]
                                                        ["normal_rate"] !=
                                                    _presenter
                                                            ?.listItems?[index]
                                                        ["promo_rate"]
                                                ? Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        'Rp. $amounts2',
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 15.0),
                                                      ),
                                                      Text(
                                                        'Rp. $amounts',
                                                        style: TextStyle(
                                                            decoration:
                                                                TextDecoration
                                                                    .lineThrough,
                                                            color: Colors.grey,
                                                            fontSize: 13.0),
                                                      ),
                                                    ],
                                                  )
                                                : Text(
                                                    'Rp. $amounts',
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15.0),
                                                  ),
                                            LikeButton(
                                              size: 25,
                                              circleColor: CircleColor(
                                                  start: Color(0xff00ddff),
                                                  end: Color(0xff0099cc)),
                                              bubblesColor: BubblesColor(
                                                dotPrimaryColor:
                                                    Color(0xff33b5e5),
                                                dotSecondaryColor:
                                                    Color(0xff0099cc),
                                              ),
                                              likeBuilder: (bool isLiked) {
                                                return Icon(
                                                  isLiked
                                                      ? Icons.favorite
                                                      : Icons.favorite_outline,
                                                  color: isLiked
                                                      ? Colors.black
                                                      : Colors.grey,
                                                  size: 25,
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                        SizedBox(height: 3),
                                        Text(
                                          _presenter?.listItems?[index]
                                              ["item_name"],
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15.0),
                                        ),
                                      ],
                                    )),
                                SizedBox(height: 15)
                              ],
                            ),
                            onTap: () {
                              // Navigator.push(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (ctx) => WisataDetails(
                              //               modelWisata: [modelWisata.item[index]],
                              //               // images: images[index],
                              //             )));
                            },
                          ),
                        ],
                      );
                    }
                  } else {
                    return Container();
                  }
                }));
  }

  @override
  void onFinishLoadingLogin() {
    EasyLoading.dismiss();
  }

  @override
  void onStartLoadingLogin() {
    EasyLoading.show(
      status: 'loading...',
      maskType: EasyLoadingMaskType.black,
    );
  }
}
