import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:tester_project/ui/main-menu/all-product.dart';
import 'package:tester_project/ui/main-menu/bola-product.dart';
import 'package:tester_project/ui/main-menu/pakaian-product.dart';

class MainMenu extends StatefulWidget {
  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> with TickerProviderStateMixin {
  List<Tab> tabList = [
    Tab(
      text: 'Semua',
    ),
    Tab(
      text: 'Bola',
    ),
    Tab(
      text: 'Pakaian',
    )
  ];
  TabController? _tabController;
  @override
  void initState() {
    // tabList.add(new Tab(
    //   text: 'Semua',
    // ));
    // tabList.add(new Tab(
    //   text: 'Bola',
    // ));
    // tabList.add(new Tab(
    //   text: 'Pakaian',
    // ));
    _tabController = new TabController(vsync: this, length: tabList.length);
    super.initState();
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.black,
              )),
          title: Center(
              child: Image.asset(
            'assets/logo.png',
            fit: BoxFit.fill,
            height: 35,
          )),
          actions: [Icon(Icons.shopping_cart)]),
      body: Column(
        children: [
          TabBar(
              controller: _tabController,
              indicatorColor: Colors.black,
              indicatorSize: TabBarIndicatorSize.tab,
              unselectedLabelColor: Colors.black,
              labelColor: Colors.white,
              overlayColor: MaterialStateProperty.all(Colors.transparent),
              indicator: BubbleTabIndicator(
                indicatorHeight: 50.0,
                indicatorColor: Colors.black,
                tabBarIndicatorSize: TabBarIndicatorSize.tab,
                // Other flags
                indicatorRadius: 15,
                // insets: EdgeInsets.only(left: 20, right: 20),
              ),
              tabs: tabList),
          SizedBox(height: 5),
          Expanded(
            child: NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (OverscrollIndicatorNotification overscroll) {
                overscroll.disallowGlow();
                return true;
              },
              child: TabBarView(
                  controller: _tabController,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    AllProduct(),
                    BolaProduct(),
                    PakaianProduct()
                  ]),
            ),
          ),
        ],
      ),
    );
  }
}
