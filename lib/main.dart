import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:tester_project/ui/landing-ui.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Test Inspirasolutech',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home: LandingUI(),
      builder: EasyLoading.init(),
    );
  }
}
