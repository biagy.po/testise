//constants url api
const String IP = "http://172.104.44.22";

//constants auth
const String URL_LOGIN = IP + "/api/method/login";
const String URL_LIST_ITEM = IP + '/api/resource/Item';

//constants for shared preferences
const String ISLOGIN = "shared-isLogin";
const String USERNAME = "shared-username";
const String PASSWORD = "shared-password";