import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tester_project/abstract/auth/login-abstract.dart';
import 'package:tester_project/view/auth/login-view.dart';
import 'package:tester_project/utility/constants.dart' as constants;
import 'package:http/http.dart' as http;

class LoginPresenter implements LoginBase {
  LoginView? _view;
  SharedPreferences? _prefs;
  Map<String, String> headers = {};
  LoginPresenter(LoginView? views) {
    this._view = views;
  }

  @override
  void onLogin(String username, String password) async {
    _view?.onStartLoadingLogin();
    _prefs = await SharedPreferences.getInstance();

    var _uri = Uri.parse(constants.URL_LOGIN);
    var _request = new http.MultipartRequest("POST", _uri);

    _request.fields['usr'] = username;
    _request.fields['pwd'] = password;
    var _response = await _request.send().timeout(const Duration(seconds: 30));
    if (_response.statusCode == 200) {
      _response.stream
          .transform(utf8.decoder)
          .transform(json.decoder)
          .listen((value) {
        try {
          dynamic data = value;
          if (data["message"] == "Logged In") {
            var sessionCookies = _response.headers['set-cookie'];
            _view?.onFinishLoadingLogin();
            _view?.showMessageSuccess("Login Berhasil!");
            _prefs?.setString(constants.ISLOGIN, sessionCookies.toString());
            _prefs?.setString(constants.USERNAME, username);
            _prefs?.setString(constants.PASSWORD, password);
          } else {
            _view?.onFinishLoadingLogin();
            _view?.showMessageFailed('Username / Password Salah!');
          }
        } on Exception catch (e) {
          print(e);
          print('Error while fetching data');
          _view?.onFinishLoadingLogin();
        }
      });
    } else {
      print(_response.statusCode);
      _view?.onFinishLoadingLogin();
      _view?.showMessageFailed('Username / Password Salah!');
    }
  }
}
