import 'package:shared_preferences/shared_preferences.dart';
import 'package:tester_project/abstract/main/main-menu-abstract.dart';
import 'package:tester_project/view/main/main-menu-view.dart';
import 'package:http/http.dart' as http;
import 'package:tester_project/utility/constants.dart' as constants;
import 'dart:convert';

class MainMenuPresenter implements MainMenuBase {
  MainMenuView? _view;
  SharedPreferences? _prefs;
  Map<String, String> headers = {};
  var listItems;
  var listimages;

  MainMenuPresenter(MainMenuView views) {
    this._view = views;
  }

  @override
  Future<void> onGetData() async {
    _view?.onStartLoadingLogin();
    _prefs = await SharedPreferences.getInstance();
    var session = _prefs?.getString(constants.ISLOGIN);
    var _uri = Uri.parse(constants.URL_LIST_ITEM +
        '?fields=["item_code","item_name","image","normal_rate","promo_rate"]');

    if (session != null) {
      int index = session.indexOf(';');
      headers['cookie'] = (index == -1) ? session : session.substring(0, index);
    }

    http.Response response = await http.get(_uri, headers: headers);
    _view?.onFinishLoadingLogin();
    // print(response.body);
    var data = jsonDecode(response.body);
    listItems = data["data"];
    int a = 0;
    for (a; a < listItems.length; a++) {
      listimages = listItems[a]["image"];
    }
  }
}
