abstract class LoginView {
  void onStartLoadingLogin();
  void onFinishLoadingLogin();
  void onNextAcivity();
  void showMessageSuccess(String value);
  void showMessageFailed(String value);
}
